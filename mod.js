const
    axios = require('axios'),
    { Window } = require('happy-dom'),
    numbro = require('numbro'),
    dayjs = require('dayjs');

module.exports = async ({
    codename,
    region
}) => {
    const
        baseURL = `https://${region ? `${region}.` : ''}dl.twrp.me`,
        { data } = await axios(
            codename,
            { baseURL }
        ),
        { document } = new Window();
    document.documentElement.innerHTML = data;
    return document.querySelectorAll('tr').map(item => {
        const itemLink = item.querySelector('a');
        return {
            url: `${baseURL}${itemLink.getAttribute('href')}`,
            name: itemLink.textContent,
            size: numbro.unformat(item.querySelector('small').textContent.toLowerCase()),
            creationTimestamp: dayjs(item.querySelector('em').textContent).valueOf()
        };
    });
};